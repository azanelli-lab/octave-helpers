## -*- texinfo -*-
## @deftypefn {} {} specgram_simple(@var{filename}, @var{Fs})
## Plot a spectogram of @var{filename}.
##
## @var{filename} should point to a file containing samples delimited by newline
## character.
function specgram_simple(filename, Fs)
  pkg load signal
  data = dlmread(filename, '\n');
  window = ceil(100 *Fs / 1000);
  step = ceil(20 * Fs / 1000);
  specgram(data, 2^nextpow2(window), Fs, window, step)
endfunction
