## -*- texinfo -*-
## @deftypefn {} {} fplotsig_fft(@var{filename}, @var{Fs})
## Plot the FFT of a raw signal from @var{filename}.
##
## @var{filename} should point to a file containing samples delimited by newline
## character.
function fplotsig_fft (filename, Fs)
  pkg load signal;

  data = dlmread(filename, '\n');
  n = length(data);
  f = Fs * (-n/2:n/2-1)/n;
  y = fftshift(abs(fft(data, n)));
  plot(f, y);
endfunction
