## -*- texinfo -*-
## @deftypefn {} {} fplotsig(@var{filename}, @var{Fs})
## Plot a raw signal from @var{filename}.
##
## @var{filename} should point to a file containing samples delimited by newline
## character.
function fplotsig (filename, Fs)
  data = dlmread(filename, '\n');
  n = length(data);
  t = (0:1/Fs:n/Fs - 1/Fs);
  plot(t, data);
endfunction
